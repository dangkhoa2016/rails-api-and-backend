class ToysController < ApplicationController
  before_action :load_toy, only: [:update, :destroy, :show]

  def index
    # @toys = Toy.order(:id)
    @toys = Toy.page(page_index).per(page_size)
    render json: @toys
  end

  def create
    @toy = Toy.new(create_toy_params)
    if @toy.validate
      @toy.save
      render json: @toy, status: 201
    else
      render json: @toy.errors.full_messages, status: 422
    end
  end

  def update
    @toy.update(edit_toy_params)
    render json: @toy
  end

  def show
    render json: @toy
  end

  def destroy
    @toy.destroy
    render json: {message: "IT HAS BEEN DELETED YAY", toy: @toy}
  end

  def random
    @random_toy = Toy.all.sample
    render json: @random_toy
  end

  private

  def load_toy
    @toy = Toy.find(params[:id])
  end

  def edit_toy_params
    params.permit(:likes)
  end

  def create_toy_params
    params.permit(:likes, :image, :name)
  end

  def page_size
    num = (params["page_size"] || '2').to_i
    if num > 10
      num = 10
    elsif num <= 0
      num = 4
    end

    num
  end

  def page_index
    (params["page_index"] || '1').to_i
  end
end
