Rails.application.routes.draw do
  root "welcome#code"
  get '/toys/random', to: 'toys#random'
  
  resources :toys
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

end
